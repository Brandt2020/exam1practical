// Zachary Brandt
// Exam 1 Practical

#include <iostream>
#include <conio.h>

using namespace std;

float Square(float num)
{
	return num * num;
}

float Cube(float num)
{
	return num * num * num;
}

int main()
{
	float userInput;
	int userChoice;
	char endLoop;

	do 
	{
		cout << "\nPlease enter a number \n";
		cin >> userInput;

		do
		{
		cout << "\nDo you want to square or cube the number you entered? Enter '2' to Square or enter '3' to Cube. \n";
		cin >> userChoice;
		} while (userChoice >3 || userChoice<2);

		if (userChoice == 2)
			cout << "\n" << userInput << " squared is " << Square(userInput);
		else
			cout << "\n" << userInput << " cubed is " << Cube(userInput);

		cout << "\nPerform another calculation? Enter 'Y' to continue, enter 'N' to end. \n";
		cin >> endLoop;

	} while (endLoop != 'N');




	_getch;
	return 0;
}